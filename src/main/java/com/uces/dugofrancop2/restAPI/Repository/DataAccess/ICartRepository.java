package com.uces.dugofrancop2.restAPI.Repository.DataAccess;

import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICartRepository extends JpaRepository<Carts, Integer> {
  
}