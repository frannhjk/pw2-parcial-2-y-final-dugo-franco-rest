package com.uces.dugofrancop2.restAPI.Repository.Models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Report {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String processedDateTime;
    private double profit;
    private int totalCartsFailed;
    private int totalCartsProcessed;
    @ManyToMany
    private Set<Products> withoutStockproducts;

    public Report(){}
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProcessedDateTime() {
        return processedDateTime;
    }

    public void setProcessedDateTime(String processedDateTime) {
        this.processedDateTime = processedDateTime;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public int getTotalCartsFailed() {
        return totalCartsFailed;
    }

    public void setTotalCartsFailed(int totalCartsFailed) {
        this.totalCartsFailed = totalCartsFailed;
    }

    public int getTotalCartsProcessed() {
        return totalCartsProcessed;
    }

    public void setTotalCartsProcessed(int totalCartsProcessed) {
        this.totalCartsProcessed = totalCartsProcessed;
    }

    public Set<Products> getWithoutStockproducts() {
        return withoutStockproducts;
    }

    public void setWithoutStockproducts(Set<Products> withoutStockproducts) {
        this.withoutStockproducts = withoutStockproducts;
    }

    public Report(String processedDateTime, double profit, int totalCartsFailed, int totalCartsProcessed,
            Set<Products> withoutStockproducts) {
        this.processedDateTime = processedDateTime;
        this.profit = profit;
        this.totalCartsFailed = totalCartsFailed;
        this.totalCartsProcessed = totalCartsProcessed;
        this.withoutStockproducts = withoutStockproducts;
    }
}