package com.uces.dugofrancop2.restAPI.Repository.Models;

import java.util.HashMap;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Carts {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String fullName;

  private String email;

  private String creationDate;

  @ManyToMany
  private Set<Products> products;

  private double total;

  private String status;

  public HashMap<Integer, Integer> quantityProdIdMap = new HashMap<Integer, Integer>();

  private String checkOutDate;

  public Carts() {

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
/*
  public Set<Products> getProducts() {
    return products;
  }

  public void setProducts(Set<Products> products) {
    this.products = products;
  }*/

  public Set<Products> getProducts() {
    return products;
  }

  public void setProducts(Set<Products> products) {
    this.products = products;
  }

  public HashMap<Integer, Integer> getQuantityProdIdMap() {
    return quantityProdIdMap;
  }

  public void setQuantityProdIdMap(HashMap<Integer, Integer> quantityProdIdMap) {
    this.quantityProdIdMap = quantityProdIdMap;
  }

  public String getCheckOutDate() {
    return checkOutDate;
  }

  public void setCheckOutDate(String checkOutDate) {
    this.checkOutDate = checkOutDate;
  }

  public Carts(String fullName, String email) {
    this.fullName = fullName;
    this.email = email;
  }


}