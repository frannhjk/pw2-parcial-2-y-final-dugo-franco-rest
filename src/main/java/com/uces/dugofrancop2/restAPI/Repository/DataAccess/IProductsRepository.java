package com.uces.dugofrancop2.restAPI.Repository.DataAccess;

import com.uces.dugofrancop2.restAPI.Repository.Models.Products;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface IProductsRepository extends JpaRepository<Products, Integer> { 
  
}