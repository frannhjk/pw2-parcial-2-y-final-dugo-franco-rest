package com.uces.dugofrancop2.restAPI.Repository.DataAccess;

import org.springframework.stereotype.Repository;

import com.uces.dugofrancop2.restAPI.Repository.Models.Report;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface IReportRepository extends JpaRepository<Report, Integer>{
    
}