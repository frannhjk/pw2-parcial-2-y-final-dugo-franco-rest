package com.uces.dugofrancop2.restAPI.Services.DTOS;

public class CarritoEditDTO {
    
    private String fullName;

    private String email;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
  
}