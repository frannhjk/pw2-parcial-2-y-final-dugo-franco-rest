package com.uces.dugofrancop2.restAPI.Services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.uces.dugofrancop2.restAPI.Repository.DataAccess.ICartRepository;
import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;
import com.uces.dugofrancop2.restAPI.Repository.Models.Products;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CarritoEditDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CartsDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ProductsDTO;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.ICartService;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProductService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartService implements ICartService {

  //#region Variables
  @Autowired
  private ICartRepository cartRepository;
  private ModelMapper mapper = new ModelMapper();
  @Autowired
  private IProductService productService;

  HashMap<Integer, Integer> mapIdQuan = new HashMap<Integer, Integer>();
  private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

  private static Logger _logger = LoggerFactory.getLogger(CartService.class);

  //#endregion


  // Guardar Carritos
  @Override
  public Carts guardarCarrito(CartsDTO cart) {

    LocalDateTime now = LocalDateTime.now();

    String creationD = now.format(formatter);

    if (cart.getCreationDate() == null)
      cart.setCreationDate(creationD);

    if (cart.getStatus() == null)
      cart.setStatus("New");

    Carts productEntity = mapper.map(cart, Carts.class);

    if (!this.mapIdQuan.isEmpty())
      this.mapIdQuan.clear();

    return this.cartRepository.save(productEntity);
  }


  // Obtener todos los carritos como DTO
  @Override
  public List<CartsDTO> getCarritos() {

    List<CartsDTO> DtoList = mapper.map(this.cartRepository.findAll(), new TypeToken<List<CartsDTO>>() {
    }.getType());

    if (DtoList.size() == 0)
      try {
        throw new Exception("There are no carts");
      } catch (final Exception e) {
      }

    return DtoList;
  }


  // Obtener todos los carritos como Entity
  @Override
  public List<Carts> getCarritosFromRepo() {

    List<Carts> cartList = this.cartRepository.findAll();

    if (cartList.size() == 0)
      try {
        throw new Exception("There are no carts");
      } catch (Exception e) {
      }

    return cartList;
  }


  // Validación de email para chequear si el carrito existe
  @Override
  public boolean Existe(CartsDTO _cart) {

    List<CartsDTO> DtoList = mapper.map(this.cartRepository.findAll(), new TypeToken<List<CartsDTO>>() {
    }.getType());

    if (DtoList.size() > 0){
      for (CartsDTO cart : DtoList) {
        
        String mail1 = cart.getEmail();
        String mail2 = _cart.getEmail();

        if (mail1.equals(mail2)) return true;
      }
    }

    return false;
  }


  // Validación si existe by Id
  @Override
  public boolean ExisteById(int cartId) {
    if (this.cartRepository.existsById(cartId))
      return true;
    return false;
  }


  // Obtener los carritos por Id
  @Override
  public Carts getCarritoById(int cartId) {

    Optional<Carts> cart = this.cartRepository.findById(cartId);

    if (cart.isPresent()) {
      Carts cartReturn = cart.get();
      return cartReturn;
    }

    return null;
  }

  // Agregar producto por Id a un carrito determinado por Id
  @Override
  public CartsDTO AgregarProductoAlCarrito(int cartId, int productId, int quantity) {

    Carts cart = this.getCarritoById(cartId);

    ProductsDTO productDTO = productService.getProductoDTOById(productId);

    Products producto = mapper.map(productDTO, Products.class);

    double total = this.calcularTotal(productId, quantity);

    cart.setTotal(cart.getTotal() + total );

    cart.getProducts().add(producto);

    mapProductoToCarrito(productId, quantity, cart);

    this.cartRepository.save(cart);
    
    CartsDTO cartDTO = mapper.map(cart, CartsDTO.class);

    cartDTO.setId(cart.getId());

    return cartDTO;
  }


  // Calcular el total del producto agregado
  @Override
  public double calcularTotal(int productId, int quantity) {
    double total;
    total = (productService.getPrecioByProducto(productId) * quantity);

    return total;
  }


  // Remover producto de un carrito by Id
  @Override
  public void removerProducto(int cartId, int productId) {
    Carts cart = this.getCarritoById(cartId);

    Products prod = mapper.map(productService.getProductoDTOById(productId), Products.class);

    cart.getProducts().removeIf(x -> x.getId() == prod.getId());

    setTotalLocal(cart, prod);

    this.cartRepository.save(cart);
  }


  // Obtiene los productos ingresados en un carrito by Id
  @Override
  public Set<ProductsDTO> getProductosDelCarrito(int cartId) {
    Carts cart = this.getCarritoById(cartId);

    Set<ProductsDTO> result = mapper.map(cart.getProducts(), new TypeToken<Set<ProductsDTO>>() {
    }.getType());

    return result;
  }


  // Checkout: Cambia estado a Ready
  @Override
  public void changeStatus(int id) {

    LocalDateTime now = LocalDateTime.now();

    String creationD = now.format(formatter);
    
    Carts cart = this.getCarritoById(id);

    if (cart.getStatus() != "New"){
      try {
        throw new Exception("Invalid Cart status");
      } catch (final Exception e) {
        e.printStackTrace();
      }
    }
    else{
      cart.setStatus("READY");

      cart.setCheckOutDate(creationD);
  
      this.cartRepository.save(cart);
    }
  }

  // Entidad Carrito to DTO
  @Override
  public CartsDTO mapperOut (Carts cart){
    return mapper.map(cart, CartsDTO.class);
  }

  // Borrar Carrito by Id
  @Override
  public void deleteCarrito(int id){
    this.cartRepository.deleteById(id);
  }

  // Editar carrito by Id
  @Override
  public CartsDTO editarCarrito(int id, CarritoEditDTO dto){

    Carts carrito = getCarritoById(id);

    carrito.setFullName(dto.getFullName());
    carrito.setEmail(dto.getEmail());
    
    this.cartRepository.save(carrito);

    CartsDTO result = this.mapperOut(carrito);

    return result;
  }
  

  // Obtener los carritos cuyo estado es == Processed
  @Override
  public List<Carts> getProcessedCarritos(){

    List<Carts> result = new ArrayList<Carts>();

    List<Carts> carritos = this.cartRepository.findAll();

    for (Carts carts : carritos) {
      
      if (carts.getStatus() == "PROCESSED"){
        result.add(carts);
      }
    }

    if (result.size() > 0)
      return result;

    return carritos;
  }

  // Obtener los carritos cuyo estado es == Failed
  @Override
  public List<Carts> getFailedCarritos(){

    List<Carts> result = new ArrayList<Carts>();

    List<Carts> carritos = this.cartRepository.findAll();

    for (Carts carts : carritos) {
      
      if (carts.getStatus() == "FAILED"){
        result.add(carts);
      }
    }

    return result;
  }

  // Obtener los carritos cuyo estado es != a New
  @Override
  public List<CartsDTO> getCarritosNotNew(){

    List<CartsDTO> result = new ArrayList<CartsDTO>();

    List<Carts> carritos = this.cartRepository.findAll();

    for (Carts carts : carritos) {
      
      if (carts.getStatus() != "NEW"){
        
        result.add(mapper.map(carts, CartsDTO.class));
        
      }
    }
    return result;
  }

  //#region Métodos privados 

  // Método para descontar el valor de la propiedad Cart.Total cuando se elimina
  // un producto de ese carrito.
  private void setTotalLocal(Carts cart, Products prod) {

    double valor = 0;

    if(cart.getQuantityProdIdMap().containsKey(prod.getId())){

      // obtener valor
      valor = this.calcularTotal(prod.getId(), cart.getQuantityProdIdMap().get(prod.getId()));

      cart.getQuantityProdIdMap().remove(prod.getId());

      double setValor = cart.getTotal() - valor;

      cart.setTotal(setValor);
    }
  }

   // Mapeo de productos agregados a los carritos
   private void mapProductoToCarrito(int productId, int quantity, Carts cart) {
    if (cart.getQuantityProdIdMap() == null) 
    {
      this.mapIdQuan.clear();

      this.mapIdQuan.put(productId, quantity);

      cart.setQuantityProdIdMap(mapIdQuan);
    }
    else 
    {
      cart.getQuantityProdIdMap().put(productId, quantity);
    }
  }

  //#endregion
}
