package com.uces.dugofrancop2.restAPI.Services;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.uces.dugofrancop2.restAPI.Repository.DataAccess.IProductsRepository;
import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;
import com.uces.dugofrancop2.restAPI.Repository.Models.Products;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ProductsDTO;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProductService;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService implements IProductService {

  // #region Variables
  @Autowired
  private IProductsRepository productRepo;
  private ModelMapper mapper = new ModelMapper();
  // #endregion


  // Guardar producto
  @Override
  public Products guardarProducto(ProductsDTO product) {

    Products productEntity = mapper.map(product, Products.class);

    if (productEntity.getStock() == 0)
      try {
        throw new Exception("No se puede ingresar u editar productos con stock en 0");
      } catch (Exception e) {
        e.printStackTrace();
      }

    return this.productRepo.save(productEntity);
  }


  // Obtener todos los DTOS
  @Override
  public List<ProductsDTO> getProductos(){

    List<ProductsDTO> DtoList = mapper.map(this.productRepo.findAll(), new TypeToken<List<ProductsDTO>>(){}.getType());

    return DtoList;  
  }

  
  // Obtener todos los productos
  @Override
  public List<Products> getProductsFromRepo(){
    return this.productRepo.findAll();
  }


  // Obtener producto por Id
  @Override
  public Optional<Products> getProductosById(int productId){
    return this.productRepo.findById(productId);
  }

  
  // Obtener productoDTO por Id
  @Override
  public ProductsDTO getProductoDTOById(int productId){

    Optional<Products> product = this.productRepo.findById(productId);

    if(product.isPresent()){
      ProductsDTO productDto = mapper.map(product.get(), ProductsDTO.class);
      return productDto;
    }

    return null;
  }

  
  // Validación producto existe por Id
  @Override
  public boolean existe(int productId){
    if(this.productRepo.existsById(productId))
      return true;
    
    return false;
  }


  // Borrar producto por Id
  @Override
  public void borrarProducto(int productId){
      this.productRepo.deleteById(productId);
  } 


  // Validación de stock suficiente
  @Override
  public boolean stockSuficiente(int productId, int quantity){

    Optional<Products> producto = this.getProductosById(productId);

    if(producto.get().getStock() < quantity)
      return false;
    
    return true;
  }


  // Obtener precio del producto por Id
  @Override
  public double getPrecioByProducto(int productId){
    
    Optional<Products> producto = this.getProductosById(productId);

    return producto.get().getUnitPrice();
  }


  // Entidad a DTO
  @Override
  public ProductsDTO mapperOut (Products cart){
    return mapper.map(cart, ProductsDTO.class);
  }

  
  // Obtener los productos sin stock
  @Override
  public Set<Products> getWithoutStockProducts(List<Carts> carritos){

    Set<Products> setResult = new HashSet<Products>();

    for (Carts cart : carritos){
      for (Products prod : cart.getProducts()){
        setResult.add(prod);
      }
    }
    
    return setResult;
  }
}