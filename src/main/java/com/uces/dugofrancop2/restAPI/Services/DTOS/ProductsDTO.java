package com.uces.dugofrancop2.restAPI.Services.DTOS;

public class ProductsDTO {
  
  private int id;
  
  private String description;

  private double unitPrice;

  private int stock;


  public ProductsDTO(){
    
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public double getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(double unitPrice) {
    this.unitPrice = unitPrice;
  }

  public int getStock() {
    return stock;
  }

  public void setStock(int stock) {
    this.stock = stock;
  }

}