package com.uces.dugofrancop2.restAPI.Services.DTOS;

import java.util.HashMap;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uces.dugofrancop2.restAPI.Repository.Models.Products;

public class CartsDTO {

  private int id;

  private String fullName;

  private String email;

  private String creationDate;

  private Set<Products> products;

  private double total;

  private String status;

  public HashMap<Integer, Integer> quantityProdIdMap;

  private String checkOutDate;

  public CartsDTO(){}

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Set<Products> getProducts() {
    return products;
  }

  public void setProducts(Set<Products> products) {
    this.products = products;
  }

  @JsonIgnore
  public HashMap<Integer, Integer> getQuantityProdIdMap() {
    return quantityProdIdMap;
  }

  public void setQuantityProdIdMap(HashMap<Integer, Integer> quantityProdIdMap) {
    this.quantityProdIdMap = quantityProdIdMap;
  }

  public CartsDTO(int id, String fullName, String email, String creationDate, double total, String status,
      Set<Products> products, HashMap<Integer, Integer> quantityProdIdMap) {
    this.id = id;
    this.fullName = fullName;
    this.email = email;
    this.creationDate = creationDate;
    this.total = total;
    this.status = status;
    this.products = products;
    this.quantityProdIdMap = quantityProdIdMap;
  }

  public String getCheckOutDate() {
    return checkOutDate;
  }

  public void setCheckOutDate(String checkOutDate) {
    this.checkOutDate = checkOutDate;
  }
}