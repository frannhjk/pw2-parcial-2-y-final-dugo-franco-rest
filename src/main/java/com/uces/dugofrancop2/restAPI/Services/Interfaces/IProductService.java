package com.uces.dugofrancop2.restAPI.Services.Interfaces;

import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;
import com.uces.dugofrancop2.restAPI.Repository.Models.Products;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ProductsDTO;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface IProductService {
  
  Products guardarProducto(ProductsDTO product);

  List<ProductsDTO> getProductos();

  List<Products> getProductsFromRepo();

  boolean existe(int productId);

  Optional<Products> getProductosById(int productId);

  void borrarProducto(int productId);

  boolean stockSuficiente(int productId, int quantity);

  double getPrecioByProducto(int productId);

  ProductsDTO getProductoDTOById(int productId);

  ProductsDTO mapperOut (Products cart);

  Set<Products> getWithoutStockProducts(List<Carts> carritos);
}