package com.uces.dugofrancop2.restAPI.Services.DTOS;

import java.util.Set;

public class ReportDTO {
    
    private int id;
    private String processedDateTime;
    private double profit;
    private int totalCartsFailed;
    private int totalCartsProcessed;
    private Set<ProductsDTO> withoutStockproducts;

    public ReportDTO(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProcessedDateTime() {
        return processedDateTime;
    }

    public void setProcessedDateTime(String processedDateTime) {
        this.processedDateTime = processedDateTime;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public int getTotalCartsFailed() {
        return totalCartsFailed;
    }

    public void setTotalCartsFailed(int totalCartsFailed) {
        this.totalCartsFailed = totalCartsFailed;
    }

    public int getTotalCartsProcessed() {
        return totalCartsProcessed;
    }

    public void setTotalCartsProcessed(int totalCartsProcessed) {
        this.totalCartsProcessed = totalCartsProcessed;
    }

    public Set<ProductsDTO> getWithoutStockproducts() {
        return withoutStockproducts;
    }

    public void setWithoutStockproducts(Set<ProductsDTO> withoutStockproducts) {
        this.withoutStockproducts = withoutStockproducts;
    }

    public ReportDTO(int id, String processedDateTime, double profit, int totalCartsFailed, int totalCartsProcessed,
            Set<ProductsDTO> withoutStockproducts) {
        this.id = id;
        this.processedDateTime = processedDateTime;
        this.profit = profit;
        this.totalCartsFailed = totalCartsFailed;
        this.totalCartsProcessed = totalCartsProcessed;
        this.withoutStockproducts = withoutStockproducts;
    }
}