package com.uces.dugofrancop2.restAPI.Services.DTOS;

public class CartProductDTO {
    
    private int id;
    private int quantity;
    private int productId;

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

	



    
}