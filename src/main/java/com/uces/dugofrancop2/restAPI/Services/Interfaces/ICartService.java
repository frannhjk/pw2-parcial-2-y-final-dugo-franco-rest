package com.uces.dugofrancop2.restAPI.Services.Interfaces;

import java.util.List;
import java.util.Set;

import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CarritoEditDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CartsDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ProductsDTO;

public interface ICartService {

  Carts guardarCarrito(CartsDTO cart);

  List<CartsDTO> getCarritos();

  boolean Existe(CartsDTO _carts);

  boolean ExisteById(int cartId);

  Carts getCarritoById(int cartId);

  CartsDTO AgregarProductoAlCarrito(int cartId, int productId, int quantity);

  double calcularTotal(int productId, int quantity);

  void removerProducto(int cartId, int productId);

  Set<ProductsDTO> getProductosDelCarrito(int cartId);

  void changeStatus(int id);
  
  List<Carts> getCarritosFromRepo();

  CartsDTO mapperOut (Carts cart);

  void deleteCarrito(int id);

  CartsDTO editarCarrito(int id, CarritoEditDTO dto);

  List<Carts> getProcessedCarritos();

  List<Carts> getFailedCarritos();
  
  List<CartsDTO> getCarritosNotNew();
}