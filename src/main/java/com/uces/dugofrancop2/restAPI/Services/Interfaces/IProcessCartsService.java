package com.uces.dugofrancop2.restAPI.Services.Interfaces;

import java.time.LocalDate;
import java.util.List;


import com.uces.dugofrancop2.restAPI.Services.DTOS.ReportDTO;

public interface IProcessCartsService {

  ReportDTO ProcessCarts();

  List<ReportDTO> GetReports(LocalDate start, LocalDate end);

  List<ReportDTO> GetAllReportes();
}