package com.uces.dugofrancop2.restAPI.Services;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.uces.dugofrancop2.restAPI.Repository.DataAccess.ICartRepository;
import com.uces.dugofrancop2.restAPI.Repository.DataAccess.IReportRepository;
import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;
import com.uces.dugofrancop2.restAPI.Repository.Models.Products;
import com.uces.dugofrancop2.restAPI.Repository.Models.Report;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ProductsDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ReportDTO;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.ICartService;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProcessCartsService;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProductService;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ProcessCartsService implements IProcessCartsService {

    // #region Variables
    private IReportRepository repo;

    @Autowired
    private ICartRepository cartRepository;
    @Autowired
    private IProductService productService;
    @Autowired
    private ICartService cartService;

    private ModelMapper mapper = new ModelMapper();

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    private static final Logger _logger = LoggerFactory.getLogger(CartService.class);
    // #endregion

    public ProcessCartsService(IReportRepository _repo){
        this.repo = _repo;
    }

    // Procesar Carritos
    @Override
    public ReportDTO ProcessCarts() {

        List<Carts> carritosToProcess = new ArrayList<Carts>();
        
        LocalDateTime now = LocalDateTime.now();

        String creationD = now.format(formatter);

        _logger.info("El BatchProcess para procesar los carritos se ha iniciado: " + creationD.toString());

        List<Carts> carritos = this.cartRepository.findAll(Sort.by(Sort.Direction.ASC, "checkOutDate"));

        for (Carts c : carritos) {
            if (c.getStatus() == "READY")
                carritosToProcess.add(c);
        }

        ProductsDTO productDTO = new ProductsDTO();

        if (carritosToProcess.size() > 0)
            procesarCarritos(carritosToProcess, productDTO);

        Report report = configureReporte(creationD);
                
        Set<ProductsDTO> outStockProductsDTOSet = mapper.map(report.getWithoutStockproducts(), new TypeToken<HashSet<ProductsDTO>>() {
        }.getType());

        _logger.info("El BatchProcess para procesar los carritos ha finalizado: " + creationD.toString());

        ReportDTO result = new ReportDTO(
            report.getId(), 
            report.getProcessedDateTime(), 
            report.getProfit(), 
            report.getTotalCartsFailed(), 
            report.getTotalCartsProcessed(),
            outStockProductsDTOSet
        );

        return result;
    }

    // Obtener Reportes entre Fechas
    @Override
    public List<ReportDTO> GetReports(LocalDate start, LocalDate end){

        //LocalDate startDate = LocalDate.parse(start);
        //LocalDate endDate = LocalDate.parse(end);
        List<ReportDTO> reportesResult = new ArrayList<ReportDTO>();

        ReportDTO dto = new ReportDTO();
        List<Report> entities = this.repo.findAll();
        List<ReportDTO> result = new ArrayList<ReportDTO>();

        for (Report entity : entities) {
            dto.setId(entity.getId());
            dto.setProcessedDateTime(entity.getProcessedDateTime());
            dto.setProfit(entity.getProfit());
            dto.setTotalCartsFailed(entity.getTotalCartsFailed());
            dto.setTotalCartsProcessed(entity.getTotalCartsProcessed());

            Set<ProductsDTO> outStockProductsDTOSet = mapper.map(entity.getWithoutStockproducts(), new TypeToken<HashSet<ProductsDTO>>() {
            }.getType());

            dto.setWithoutStockproducts(outStockProductsDTOSet);

            result.add(dto);
        }
        
        for (ReportDTO reportDTO : result) {
            
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            String dateString = reportDTO.getProcessedDateTime();
             
            LocalDate localDateObj = LocalDate.parse(dateString, dateTimeFormatter);    //String to LocalDate
             
            if (localDateObj.isAfter(start) && localDateObj.isBefore(end)){
                reportesResult.add(reportDTO);
            }

        }

        return reportesResult;
    }

    // Obtener todos los reportes
    @Override
    public List<ReportDTO> GetAllReportes(){
        
        ReportDTO dto = new ReportDTO();
        List<Report> entities = this.repo.findAll();
        List<ReportDTO> result = new ArrayList<ReportDTO>();

        for (Report entity : entities) {
            dto.setId(entity.getId());
            dto.setProcessedDateTime(entity.getProcessedDateTime());
            dto.setProfit(entity.getProfit());
            dto.setTotalCartsFailed(entity.getTotalCartsFailed());
            dto.setTotalCartsProcessed(entity.getTotalCartsProcessed());

            Set<ProductsDTO> outStockProductsDTOSet = mapper.map(entity.getWithoutStockproducts(), new TypeToken<HashSet<ProductsDTO>>() {
            }.getType());

            dto.setWithoutStockproducts(outStockProductsDTOSet);

            result.add(dto);
        }

        return result;

    }


    //#region Private Methods

    // Procesar Carritos
    private void procesarCarritos(List<Carts> carritos, ProductsDTO productDTO){

        Integer acum = 0;

        for (Carts cart : carritos) {

            if (cart.getStatus() != "PROCESSED" && cart.getStatus() != "NEW" && cart.getProducts().size() > 0) {

                if (!cart.getQuantityProdIdMap().isEmpty()) {

                    for (Map.Entry<Integer, Integer> entry : cart.getQuantityProdIdMap().entrySet()) {

                        for (Products prod : cart.getProducts()) {
                            productDTO = this.productService.getProductoDTOById(prod.getId());

                            Integer key = entry.getKey();
                            Integer value = entry.getValue();

                            if (key == productDTO.getId())
                                acum = acum + value;
                        }
                    }
                    if (productDTO.getStock() < acum) {
                        cart.setStatus("FAILED");
                        _logger.error(
                                "El Status del carrito fue FAILED porque no se encontró suficiente stock del producto");
                    }
                    if (productDTO.getStock() >= acum) {
                        cart.setStatus("PROCESSED");
                        _logger.trace(
                                "El Status del carrito fue PROCESSED porque había stock suficiente de los productos");
                    }
                }
            } else {
                _logger.warn("El carrito que ya tiene estado PROCESSED o no posee Productos para procesar");
            }
            this.cartRepository.save(cart);
        }
    }

    // Setear Reportes
    private Report configureReporte(String processedDate){
        
        double _profit = 0;
        int _totalCartsFailed = 0;
        int _totalCartsProcessed = 0;
        Set<Products> _withoutStockProducts;
        
        List<Carts> carritosProcesado = cartService.getProcessedCarritos();
        List<Carts> carritosFailed = cartService.getFailedCarritos();

        _totalCartsFailed = carritosFailed.size();
        _totalCartsProcessed = carritosProcesado.size();
        _withoutStockProducts = productService.getWithoutStockProducts(carritosFailed);

        for (Carts carts : carritosProcesado) {
            _profit = _profit + carts.getTotal();
        }

        Report report = new Report(processedDate, _profit, _totalCartsFailed, _totalCartsProcessed, _withoutStockProducts);

        this.repo.save(report);

        return report;
    }

    //#endregion
}