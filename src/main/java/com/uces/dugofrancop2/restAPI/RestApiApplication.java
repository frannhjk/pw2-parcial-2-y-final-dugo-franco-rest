package com.uces.dugofrancop2.restAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
	}


	/* Mas Información <SWAGGER DOC>: localhost:8080/swagger-ui.html */
	/* Información de commits :  https://bitbucket.org/frannhjk/pw2-java-parcial-2-dugo-franco */

	/* ####### Notas Final ####### */ 
	
	/* Nota: Adicionalmente se entrega colección de postman */

	/* NOTA no cuento con las variables de ambiente cuando realicé el importar de la colección subida 
	para el parcial 2, sin embargo probé todo con la colección creada por mi */
	
}
	