package com.uces.dugofrancop2.restAPI.Controllers;

import java.util.List;

import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CarritoEditDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CartProductDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CartsDTO;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.ICartService;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProductService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carts")
public class CartsController extends BaseController{

  //#region Variables
  @Autowired
  private ICartService cartService;

  @Autowired
  private IProductService productService;

  private ModelMapper mapper = new ModelMapper();
  //#endregion


  public CartsController(){}
  

  // Crear un carrito
  @PostMapping
  public ResponseEntity<?> PostCarrito(@RequestBody CartsDTO cart){

    if(this.cartService.Existe(cart) == true)
      return new ResponseEntity<>("Esa dirección de correo ya existe", HttpStatus.BAD_REQUEST);

    if(cart.getFullName() == null || cart.getEmail() == null || cart.getFullName() == "" || cart.getEmail() == "")
      return new ResponseEntity<>("Invalid data to create cart", HttpStatus.BAD_REQUEST);

    Carts carrito = this.cartService.guardarCarrito(cart);
    
    if(carrito == null)
      return new ResponseEntity<>("Invalid data to create cart", HttpStatus.BAD_REQUEST);

    CartsDTO result = cartService.mapperOut(carrito);

    return new ResponseEntity<>(result, HttpStatus.CREATED);
  }


  // Agregar un Producto a un carrito
  @PostMapping("/{id}/products")
  public ResponseEntity<?> PostProductoCarrito(@RequestBody CartProductDTO cartProductDTO,
   @PathVariable int id){

    int quantity = cartProductDTO.getQuantity();
    int productToAddId = cartProductDTO.getProductId();

    //#region Exceptions
    if(productToAddId == 0 || quantity == 0 || cartProductDTO == null)
      return new ResponseEntity<>("Invalid data to add product", HttpStatus.BAD_REQUEST);

    if(!this.productService.existe(productToAddId))
      return new ResponseEntity<>("Invalid product", HttpStatus.BAD_REQUEST);

    if(!this.cartService.ExisteById(id))
      return new ResponseEntity<>("Invalid cart", HttpStatus.NOT_FOUND);

    if(this.productService.stockSuficiente(productToAddId, quantity) == false)
      return new ResponseEntity<>("Insufficient stock", HttpStatus.BAD_REQUEST);
    
    //#endregion

    CartsDTO result = this.cartService.AgregarProductoAlCarrito(id, productToAddId, quantity);

    return new ResponseEntity<>(result, HttpStatus.CREATED);
  }


  // Borrar Producto de un carrito
  @DeleteMapping("/{id}/products/{productId}")
  public ResponseEntity<?> DeleteProductoCarrito(@PathVariable int id, @PathVariable int productId){

    if(!this.productService.existe(productId))
      return new ResponseEntity<>("Invalid Product " + productId, HttpStatus.NOT_FOUND);

    if(!this.cartService.ExisteById(id))
      return new ResponseEntity<>("Invalid cart " + id, HttpStatus.NOT_FOUND);

    this.cartService.removerProducto(id, productId);

    return new ResponseEntity<>(HttpStatus.OK);
  }
  

  // Obtener Productos por carrito Id
  @GetMapping("/{id}/products")
  public ResponseEntity<?> GetProducts(@PathVariable int id){

    if(!this.cartService.ExisteById(id))
      return new ResponseEntity<>("Invalid cart", HttpStatus.NOT_FOUND);
    
    return new ResponseEntity<>(this.cartService.getProductosDelCarrito(id), HttpStatus.OK);
  }


  // Obtener Carrito por Id
  @GetMapping("/{id}")
  public ResponseEntity<?> GetCarrito(@PathVariable int id){
  
    if(!this.cartService.ExisteById(id))
      return new ResponseEntity<>("Invalid cart", HttpStatus.NOT_FOUND);

    CartsDTO dto =  mapper.map(this.cartService.getCarritoById(id), CartsDTO.class);
    
    return new ResponseEntity<>(dto, HttpStatus.OK);
  }


  // Checkout carritos por Id
  @PostMapping("/{id}/checkout")
  public ResponseEntity<?> Checkout(@PathVariable int id){

    if(!this.cartService.ExisteById(id))
      return new ResponseEntity<>("Invalid cart", HttpStatus.NOT_FOUND);    

    this.cartService.changeStatus(id);
    
    return new ResponseEntity<>(HttpStatus.OK);
  }


  // Obtener todos los carritos
  @GetMapping
  public ResponseEntity<?> GetCarritos(){
    List<CartsDTO> carritos = this.cartService.getCarritos();

    if (carritos.size() <= 0 || carritos == null)
      return new ResponseEntity<>("The are no carts", HttpStatus.NOT_FOUND);

    return new ResponseEntity<>(carritos, HttpStatus.OK);
  }


  // Borrar Carrito por Id
  @DeleteMapping("/{id}")
  public ResponseEntity<?> DeleteCarrito(@PathVariable int id){

    if(!this.cartService.ExisteById(id))
      return new ResponseEntity<>("Invalid cart", HttpStatus.NOT_FOUND);    

    this.cartService.deleteCarrito(id);

    return new ResponseEntity<>(HttpStatus.OK); 
  }


  // Editar Carrito por Id
  @PutMapping("/{id}")
  public ResponseEntity<?> PutCarrito(@RequestBody CarritoEditDTO dto, @PathVariable int id)
  {
    if (dto.getFullName() == null || dto.getFullName() == "" || dto.getEmail() == "" || dto.getEmail() == null)
      return new ResponseEntity<>("El nombre y el mail son datos requeridos ", HttpStatus.BAD_REQUEST);

    if(!this.cartService.ExisteById(id))
      return new ResponseEntity<>("No se encontro un producto con el ID: " + id, HttpStatus.NOT_FOUND);

    CartsDTO result = this.cartService.editarCarrito(id, dto);

    if (result != null)
      return new ResponseEntity<>(result, HttpStatus.OK);
    else
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

}