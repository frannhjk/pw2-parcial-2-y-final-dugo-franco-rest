package com.uces.dugofrancop2.restAPI.Controllers;

import java.time.LocalDate;
import java.util.Date;

import com.uces.dugofrancop2.restAPI.Services.DTOS.ReportDTO;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.ICartService;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProcessCartsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/batch/processCarts")
public class ProcessCartsController extends BaseController{

  @Autowired
  private IProcessCartsService service;


  public ProcessCartsController(){}
  

  // Get All Reportes
  @GetMapping()
  public ResponseEntity<?> GetProcessCarts(){

    try{
      return new ResponseEntity<>(this.service.GetAllReportes(),HttpStatus.OK);
    }
    catch(Exception e){
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
  
  // Get Reportes con fecha desde y hasta, recibida por parámetro
  @GetMapping("/date")
  public ResponseEntity<?> GetProcessCarts(
    @RequestParam("from") @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate fromDate,
    @RequestParam("to") @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate toDate){

    try{
      return new ResponseEntity<>(this.service.GetReports(fromDate, toDate),HttpStatus.OK);
    }
    catch(Exception e){
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }  

  // Procesar Carritos en en estado READY
  @PostMapping
  public ResponseEntity<?> ProcessCarts(){

    ReportDTO result = this.service.ProcessCarts();

    try{
      return new ResponseEntity<>(result, HttpStatus.OK);
    }
    catch(Exception e){
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }
  

}