package com.uces.dugofrancop2.restAPI.Controllers;

import com.uces.dugofrancop2.restAPI.Repository.Models.Products;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ProductsDTO;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/products")
public class ProductsController extends BaseController{

  @Autowired
  private IProductService productService;

  public ProductsController(){
  }

  // Obtener todos los Productos
  @GetMapping
  public ResponseEntity<?> getProducts(){
    return new ResponseEntity<>(productService.getProductos(), HttpStatus.OK);
  }


  //Obtener Producto por Ids
  @GetMapping("/{id}")
  public ResponseEntity<?> getProductById(@PathVariable int id){

    if(!this.productService.existe(id))
      return new ResponseEntity<>("No se encontro un producto con el ID: " + id, HttpStatus.NOT_FOUND);
    
    return new ResponseEntity<>(this.productService.getProductosById(id), HttpStatus.OK);
  }


  // Agregar Producto
  @PostMapping
  public ResponseEntity<?> postProduct(@RequestBody ProductsDTO product){

    //#region No Descripción - No Stock - No Producto Exception
    if(product.getDescription() == null || product.getDescription() == "" 
    || product.getStock() == 0 || product.getUnitPrice() == 0)
      return new ResponseEntity<>("Debe completar los campos requeridos", HttpStatus.BAD_REQUEST);
    
    //#endregion

    Products entity = productService.guardarProducto(product);

    if(entity == null)
      return new ResponseEntity<>("La entidad fue null", HttpStatus.BAD_REQUEST);

    ProductsDTO result = productService.mapperOut(entity);

    return new ResponseEntity<>(result, HttpStatus.CREATED);
  }


  // Editar Producto por Id
  @PutMapping("/{id}")
  public ResponseEntity<?> putProduct(@RequestBody ProductsDTO product, @PathVariable int id){
    
    if(!this.productService.existe(id))
      return new ResponseEntity<>("No se encontro un producto con el ID: " + id, HttpStatus.NOT_FOUND);
  
    product.setId(id);  
    productService.guardarProducto(product);
  
    return new ResponseEntity<>(product, HttpStatus.OK);
  }


  // Borrar Producto por Id
  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteProduct(@PathVariable int id){

    if(!this.productService.existe(id))
      return new ResponseEntity<>("No se encontro un producto con el ID: " + id, HttpStatus.NOT_FOUND);

    this.productService.borrarProducto(id);

    return new ResponseEntity<>(HttpStatus.OK);
  }
  
}