package com.uces.dugofrancop2.restAPI;

import com.uces.dugofrancop2.restAPI.Repository.DataAccess.ICartRepository;
import com.uces.dugofrancop2.restAPI.Repository.DataAccess.IProductsRepository;
import com.uces.dugofrancop2.restAPI.Repository.Models.Carts;
import com.uces.dugofrancop2.restAPI.Repository.Models.Products;
import com.uces.dugofrancop2.restAPI.Services.DTOS.CartsDTO;
import com.uces.dugofrancop2.restAPI.Services.DTOS.ProductsDTO;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.ICartService;
import com.uces.dugofrancop2.restAPI.Services.Interfaces.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.h2.fulltext.FullText;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.jupiter.*;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

@SpringBootTest
public class ShoppingCartServicesTest{

    //@Autowired
    //private ICartRepository cartRepository;

    //@Autowired
    //private IProductsRepository productRepository;

    @Autowired
    private ICartService cartService;

    @Autowired
    private IProductService productService;

    private ModelMapper mapper = new ModelMapper();


    public ShoppingCartServicesTest(){
    }


    @Test
    public void obtenerTodosLosCarritosYProductosExistentes(){

        Carts cart = new Carts("Carrito 1", "carritouno@test.com");

        CartsDTO dto = mapper.map(cart, CartsDTO.class);

        this.cartService.guardarCarrito(dto);

        Products prod = new Products("Producto 1 Descripción", 100.0, 10);

        ProductsDTO dtoProd = mapper.map(prod, ProductsDTO.class);

        this.productService.guardarProducto(dtoProd);

        List<Carts> carts = this.cartService.getCarritosFromRepo();
        List<Products> prodList = this.productService.getProductsFromRepo();

        assertFalse(carts.isEmpty());
        assertFalse(prodList.isEmpty());
    }
    
    @Test
    public void ExisteSoloUnCarrito(){

        Carts cart = new Carts("Carrito 1", "carritouno@test.com");

        cart.setStatus("PROCESSED");

        //CartsDTO dto = mapper.map(cart, CartsDTO.class);

        List<Carts> carritos = cartService.getCarritosFromRepo();

        assertTrue(carritos.size() == 1);
    }

    @Test
    public void obtenerSoloUnCarritoProcessed(){

        Carts cart = new Carts("Carrito 1", "carritouno@test.com");

        cart.setStatus("PROCESSED");

        CartsDTO dto = mapper.map(cart, CartsDTO.class);

        this.cartService.guardarCarrito(dto);

        List<Carts> carritos = cartService.getCarritosFromRepo();

        assertTrue(carritos.size() == 1);

        assertTrue(carritos.get(0).getStatus() == "PROCESSED");
    }

    @Test
    public void obtenerNingunCarritoFailed(){
        
        List<Carts> carritos = cartService.getCarritosFromRepo();

        for (Carts carrito : carritos) {
            assertTrue(carrito.getStatus() != "FAILED");
        }
    }

    @Test
    public void obtenerSoloTresProductos(){
        
        Products prod = new Products("Producto 1 Descripción", 100.0, 10);
        Products prod2 = new Products("Producto 2 Descripción", 200.0, 20);
        Products prod3 = new Products("Producto 3 Descripción", 300.0, 30);

        ProductsDTO dtoProd = mapper.map(prod, ProductsDTO.class);
        ProductsDTO dtoProd2 = mapper.map(prod2, ProductsDTO.class);
        ProductsDTO dtoProd3 = mapper.map(prod3, ProductsDTO.class);
        

        this.productService.guardarProducto(dtoProd);
        this.productService.guardarProducto(dtoProd2);
        this.productService.guardarProducto(dtoProd3);

        List<Products> prodList = this.productService.getProductsFromRepo();

        assertTrue(prodList.size() == 3);
    }

    @Test
    public void soloUnProductoSinStock(){

        int cont = 0;
        
        List<Products> productos = this.productService.getProductsFromRepo();

        if(productos.size() > 0){
            for (Products products : productos) {
                if(products.getStock() == 0){
                    cont++;
                }
            }
        }
        assertTrue(cont == 1);
    }
}